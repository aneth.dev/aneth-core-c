#include "InetSocketAddress.h"
#include "aneth/io/posix/InetSocketAddress.h"

/*
@startuml
!include InetAddress.c
namespace aneth.io {
	interface InetSocketAddress extends SocketAddress {
		+ InetAddress address()
		+ int port()
		{static} InetSocketAddress create(char* address, int port)
	}
}
@enduml
*/

InetSocketAddress InetSocketAddress_create(char* address, int port) {
	return InetSocketAddress_cast(new_aneth_io_posix_InetSocketAddress(address, port));
}
