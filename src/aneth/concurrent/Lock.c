#include "Lock.h"

/*
@startuml
!include Condition.c
interface aneth.concurrent.Lock {
	+ void lock()
    + Condition newCondition()
	+ bool tryLock()
	+ void unlock()
}
@enduml
*/

