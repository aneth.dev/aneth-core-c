#include "List.h"

/*!
@startuml
!include Collection.c
namespace aneth {
    interface List<T extends Object> extends Collection {
        + T get(size_t position)
		+ removeAt(size_t position) <<default>> <<optional>>
        + set(size_t position, T element) <<default>> <<optional>>
	}
!ifdef aneth_List
	note right of List::get
		Return the element at the mentioned position.
	end note
	note right of List::set
		Set the element at the given position.
	end note
!endif
}
@enduml
*/

void List_set(List* list, size_t position, aneth_Object element) {
	check(0, UnsupportedOperationException, "Last.set()");
}

void List_removeAt(List* list, size_t position) {
	check(0, UnsupportedOperationException, "List.removeAt()");
}
