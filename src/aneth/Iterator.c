#include "Iterator.h"

/*!
@startuml
namespace aneth {
	interface Iterator<T extends Object> {
		+ bool hasNext()
		+ T next()
		+ remove(T element) <<default>>
	}
}
@enduml
*/

void Iterator_remove(Iterator *iterator, aneth_Object element) {
	check(0, UnsupportedOperationException, "Iterator.remove()");
}


