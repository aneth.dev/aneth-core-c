#include "ByteChannel.h"

/*
@startuml
!include Channel.c
!include Readable.c
!include Writable.c
namespace aneth.io {
	interface ByteChannel extends Channel, Readable, Writable {
	}
}
@enduml
*/
