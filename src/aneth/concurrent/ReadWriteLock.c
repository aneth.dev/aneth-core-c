#include "ReadWriteLock.h"

/*
@startuml
!include Lock.c
interface aneth.concurrent.ReadWriteLock {
    + Lock readLock()
    + Lock writeLock()
}
@enduml
*/
