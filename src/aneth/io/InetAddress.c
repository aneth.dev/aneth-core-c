#include "InetAddress.h"
#include "aneth/io/posix/InetAddress.h"

/*
@startuml
namespace aneth.io {
	interface InetAddress {
		+ char* hostname()
		+ uint8_t* address()
		+ size_t size()
		{static} InetAddress create(char* address)
	}
}
@enduml
*/

InetAddress InetAddress_create(char* address) {
	return InetAddress_cast(new_aneth_io_posix_InetAddress(address));
}

