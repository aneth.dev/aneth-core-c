#include "Number.h"

/*
@startuml
namespace aneth {
	interface Number {
		+ long signedLongValue()
		+ int signedValue()
		+ unsigned unsignedValue()
		+ uint64_t unsignedLongValue()
		+ float floatValue()
		+ double doubleValue()
	}
}
@enduml
*/
