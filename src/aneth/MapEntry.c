#include "MapEntry.h"

/*
@startuml
namespace aneth {
	interface MapEntry<K extends Object,V extends Object> {
        + K getKey()
        + V getValue()
        + void setValue(V value)
	}
}
@enduml
*/
